# Tilt Brush - Réalité Virtuel

## Présentation

*[Tilt Brush](https://www.tiltbrush.com/) by Google*

Tilt Brush est une **application de peinture artistique** sur PC développée par Google. <br>
C'est une application à la première personne de création d’œuvre dans un volume en 3D en réalité virtuelle.

sortie le 4 avril 2016 en même temps que le HTC Vive, plusieurs artistes ont collaboré avec Google (*Dustin Yellin, Andrea Blasich, Christoph Niemannet, Seung Yul Oh* et d'autres), lors de la *Pioneer Works Village Fête*. <br>
Après la mort de sa plateforme Poly en fin d’année (qui permettait notamment de stocker ses créations Tilt Brush), Google annonce la mise à le retraite de Tilt Brush, **le code est passé en open source et disponible sur GitHub sous licence Apache 2.0.**

- ### Dynamic brushes

    From ink and smoke to snow and fire, Brush materials and shaders. <br>
    Different camera paths to follow you for the third person and different way to clip and take pictures of your creations.

- ### Feedback

  - left-handed support <br>
    when you enter the sketch it shows how the artwork was made <br>
    feels like your in your creation <br>
    just a few minutes of tutorials and we are able to start creating

  - add spray paint <br>
    add sculpt brush <br>


### Popularité

1. Record Guiness Book, [Derek Westerman passe 25h dans un casque de réalité virtuelle](https://www.lavenir.net/cnt/dmf20160505_00821877/il-passe-25h-dans-un-casque-de-realite-virtuelle-et-en-perd-son-esprit-video) 7 avril 2016 sur Tilt Brush

2. ![Andrew Bell](images/detail-andrew-bell-01-i.jpg)
