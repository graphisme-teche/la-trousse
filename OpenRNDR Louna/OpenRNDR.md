# **OPENRNDR**
### Histoire
Formé par Jeroen Barendse du studio *Lust* (Jeroen Barendse, Thomas Castro et Dimitri Nieuwenhuizen, La Haye (NL), 1995-2017, dissolu suite au décès de Dimitri Nieuwenhuizen), *studio RNDR* est un collectif de design interactif néerlandais.
**<https://lust.nl>**
**<https://rndr.studio>**
Jeroen Barendse a créé ce studio avec quelques-uns de ses anciens étudiants, des développeurs, graphistes et scientifiques spécialisés en numérique, pour la plupart des anciens collaborateurs de Lust. Ils s'entourent beaucoup de stagiaires, de designers freelance, et diffusent leur manière de travailler via de nombreux workshops. Les champs de travail de RNDR sont très divers.

"Design interactif, data visualisation, systèmes d’informations, logiciels, installations interactives, communication architecturale, expériences immersives, design d’interface, identités visuelles, vidéo générative, code créatif, design d’exposition, systèmes graphiques, plateformes et espaces hybrides, intelligence artificielle, workshops de design et code"
*extrait du texte de présentation (traduit en français), **https://rndr.studio/#studio***

### Outil
Officiellement lancé en 2018 mais en développement au sein du studio Lust depuis 2010, *OPENRNDR* est leur outil open source de conception, qu'ils ont développé eux-mêmes. Il a été récompensé par le *Dutch Design Award en 2019*. Disponible sur un GitHub Repository, il est amélioré et mis à jour régulièrement. 
Doté d'un fort potentiel interactif, OPENRNDR possède une vision du logiciel assez unique : il est considéré achevé lorsque l'un de ses utilisateurs finalise son propre projet sur le logiciel. Autogéré et indéfini dans sa finalité, OPENRNDR se place en opposition totale aux logiciels payants (surtout le monopole de la suite Adobe sur le design graphique), et responsabilise son utilisateur en l'incluant dans le cycle de vie de l'outil. 

"OPENRNDR est notre projet en cours le plus long et le plus important, il est crucial pour la plupart de nos projets. OPENRNDR est un outil de création d'outils, un cadre open source pour le code créatif, écrit en *langage Kotlin (NDR : les premières version d'OPENRNDR étaient codées en Java 8, Kotlin est un langage open source compatible avec les librairies JavaScript)*, qui simplifie l'écriture  de structures interactives en temps réel."
*extrait du texte de présentation d'OPENRNDR (traduit en français), **https://rndr.studio/projects/openrndr***

Léger et multiplateformes, l'objectif d'OPENRNDR est de créer une interface à la fois fiable et rapide pour coder des prototypes, et assez résistante et puissante pour finaliser des projets de design interactif qualitatifs et exploitables à grande échelle. 

### Langage Kotlin ?
Langage open source créé en 2011 *(NDR : ce qui explique l'utilisation de Java 8 pour les débuts d'OPENRNDR en 2O10)* par l'éditeur de logiciels *JetBrains*, Kotlin est un langage de programmation orienté objet et fonctionnel. Ayant été développé à Saint-Pétersbourg, son nom vient de l'île de Kotline qui fait face à la ville.