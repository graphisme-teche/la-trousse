# La trousse

Liste d'outils non-conventionnel et inconnu pour vous pour la création graphique. Cette documentation réalisée en commun avec les étudiant.es de troisième années design graphique à l'esad Valence fait l'état des connaissances accumulées durant un semestre. Les étudiant.es ont choisi un outil de conception inconnu pour elleux ou non-conventionnel. L'enjeu et de développer une trousse à outil sortant des sentiers battu par les logicielles et techniques traditionnel de conception graphique. Chaque outil est documenté et chaque présentation est accompagnée de tests formels ainsi que de la réalisation d'affiches réalisées avec ces outils.

## Documentation

- Créez un dossier portant le nom de l'outil sur votre ordinateur ;
- À l'intérieur de ce répertoire créez un dossier `Images`;
- Rédigez une dans un fichier markdown la documentation concernant cet outil ;

### Tuto markdown

Markdown  est un langage de balisage léger créé en 2004 par John Gruber avec l'aide d'Aaron Swartz. Il a été créé dans le but d'offrir une syntaxe facile à lire et à écrire.

<https://www.markdowntutorial.com/fr/>

#### Balisage

    *** Titres ***
    **************
    # Titre 1
    ## Titre 2
    ### Titre 3
    ####  Titre 4

    *** Paragraphe ***
    ******************

    Paragraphe s'écrit normalement

    *** Italique et Gras ***
    ************************

    **Gras**

    *Italique*

    *** Citations ***
    ****************

    > Citation

    *** Listes ***
    **************

    - Élément
    - Élément
    - Élément

    1. Élément
    2. Élément
    3. Élément

    *** Code ***
    ************

    Bloc de code
    ************

    Pour afficher un bloc de code, sautez deux lignes comme pour un paragraphe,
    puis indentez avec 4 espaces ou deux tabulations.

    Code inline
    ***********

    `code`

    *** Liens ***
    ************

    <https://wikipedia.com>

    [Wikipedia] (https://wikipedia.com)

    *** images ***
    **************

    ![Légende](/dossier_image/nom_image.jpg)

## Git

1. Créez un compte gitlab avec votre adresse mail esad-gv

2. Intallez git sur votre ordinateur :

  1. pour Mac OS X :
  téléchargez git ici, <https://sourceforge.net/projects/git-osx-installer/> ;

  2. Pour windows :
  téléchargez git ici, <https://git-scm.com/download/win>

3. Paramètre git :

  1. Ouvrez un terminal dans Visual Studi Code ou votre terminal ;

  2. Tapez les commandes suivantes :
  `git config --global user.name "Your Name"`
  et
  `git config --global user.email "youremail@domain.com"`

  3. Clonez le répertoire dans votre ordi :
  `cd chemin_du_dossier`
  'git clone https://gitlab.com/graphisme-teche/la-trousse.git'

4. Pushez votre contribution :

  1. Glissez votre dossier de documentation dans le dossier cloner ;

  2. Faîtes un `git pull` dans le répertoire à l'aide du terminal ;

  3. "Pushez" votre dossier :
     - dans le terminale, assurez votre d'être dans le bon dossier.
     - tapez la commande `git add .` ;
     - ensuite tapez la commande `git commit -m "entrez votre commentaire"` ;
     - enfin tapez la commande 'git push'
