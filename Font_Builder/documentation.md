# Font Builder

Font Builder est une application en ligne disponible sur le site de la fonderie *Pretend Fondery*, fonderie créée en 2019 par Helen Rice et Josh Nissenboim, du studio de design américain *Fuzzco*.

![Hello](/Images/Page_accueil.png)

L'application permet de dessiner sa propre font à main levée à l'aide du pad ou de la souris. Les outils sont restrints puisqu'il est seulement possible de se servir d'un pinceau rond avec trois diamètres différents. Le but est de dessiner chaque glyph en dix secondes maximum. Par la suite, il est possible de revenir sur chacun des dessins afin de l'améliorer.

![Hello](/Images/Presentation_dessin.png)

ll est en suite possible de nommer la font et de l'exporter au format *True Type Font* afin de la rendre exploitable pour quelconque utilisation.

![Hello](/Images/Presentation_typo.png)

Il est aussi possible d'avoir accès aux typographies dessinées à l'aide de l'application par d'autres personnes. Comme pour une font classique, il y a son nom, une présentation de chaque glyph, ainsi que la possibilité de taper différents mots avec.

![Hello](/Images/Presentation_typo_gens.png)