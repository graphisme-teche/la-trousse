# TeX / LaTeX 

![légende](IMG/TeX_logo.svg.png) 

## 1 Introduction (Qu'es-ce que c'est)

### 1.1 TeX

TeX est un logiciel de composition de document, il est à l'origine de LaTeX. Il est adapté au milieu académique et technique (Il supporte des forme de dessins peu conventionel tel que les formules mathématiques, ou les partition de musique).
Pour nous, il permet la création de plusieurs type de classe de document : Lettre, Article, Report, Livre et Présentation (slide).

C'est un logiciel WYMIWYG, What you mean is what you get :
Il prends en entrée un document écrit en TeX et créer un fichier au format .dvi

C'est un language balisé. Les commandes commencent par une contre oblique "\"
Les commandes reconnues par TeX sont divisée en "primitives" et "macro" (créer à partir des primitives), les macros permettent des structures plus complexe.

### 1.2 Différence entre TeX et LaTeX

TeX est à la fois un programme et un format, Il permet les bases de la composition de texte et de livres.

LaTeX permet plus de control à l'aide de diffenrentes commande en macro pour implanter des choses comme des sections, des titres de pages, des bibliographies, ect.

Avec TeX il est plus question mise en page, Avec LaTeX il est aussi question du contenu

## 2 Hisoire (Par qui ?)

### 2.1 Origine du programe

Lorsque le premier volume de The Art of Computer Programming fut publié en 1968, le premier livre de Donald Knuth, celui-ci fut d'abord imprimé par composition par métal chaud, une technique datant du xixe siècle qui donnait un caractère « ancien » apprécié par Knuth. Cependant lors de la seconde édition du second volume en 1976, le livre entier a dû être recomposé parce que les éditeurs utilisaient une nouvelle technique appelée photocomposition qui ne fonctionnait plus avec les anciennes polices. En effet, les anciennes imprimeries avaient été remplacées par des imprimeries à impression photographique. Lorsque Knuth reçut les premiers essais de son deuxième livre, il les trouva horribles3. Tant et si bien qu'il décida de résoudre le problème et de réaliser lui-même son outil d'édition typographique qui ne serait d'aucun ressort photographique mais bien informatique.

Dès le départ, le système TeX fut conçu afin d'être :
ergonomique, le système étant conçu afin que les auteurs puissent directement l'utiliser, c'est-à-dire les personnes sans grande connaissance en informatique ;
gratuit, le logiciel provenant d'une source universitaire.
Durant le développement, le logiciel devint indépendant par rapport à son support. Pour un même fichier d'entrée, il délivre le même fichier de sortie quel que soit l'ordinateur ou système d'exploitation sur lequel il est installé.

Donald Knuth a répété plusieurs fois que le code source de TeX était dans le domaine public18 et qu'il encourageait fortement les modifications de celui-ci. D'autre part, bien qu'il soit dans le domaine public, Knuth demande que, lorsqu'une modification de TeX est publiée, celle-ci porte une autre dénomination que TeX, pour permettre de la distinguer (exemple avec LaTeX ou ConTeXt).

### 2.2 Donald Knuth 

![légende](IMG/KnuthAtOpenContentAlliance.jpg)

**Donald Knuth** (10 janvier 1938 - Milwaukee USA) est à l'origine du développement de TeX, il informaticien et mathématicien. Il est marié à Nancy Jill Carter, qui a publié un livre sur la liturgie et réalisé les illustrations du livre sur les nombres surréels.

Knuth est surtout connu comme l'auteur de l'ouvrage The Art of Computer Programming (TAOCP), une des références dans le domaine de l'informatique.

*Fun Fact : Il a un petit humour de developpeur :), en autres les numéros de version de TeX convergent vers pi, c’est-à-dire que les versions se suivent de la sorte : « 3 », « 3,1 », « 3,14 ».*

Les travaux de Donald Knuth concernent particulièrement l'algorithmique et les mathématiques discrètes, mais il a aussi créé des logiciels très utilisés encore aujourd'hui, TeX et Metafont.

### 2.3 Parenthèse sur Metafont 

![légende](IMG/Computermodern.png)

Metafont est un language pour composer des polices vectorielles, supporter sur TeX/LaTeX, Il a notament créer la police "Computer Moderne", police par defaut sur TeX.

### 2.4 Leslie Lamport 

![légende](IMG/Lamport.jpg)

**Leslie Lamport** (7 février 1941 - New York USA) est à l'origine du developpement de LaTeX. LaTeX permet de rédiger des documents dont la mise en page est réalisée automatiquement en se conformant du mieux possible à des normes typographiques. Une fonctionnalité distinctive de LaTeX est son mode mathématique, qui permet de composer des formules complexes (LaTeX eu sa première version en 1983 (dépot sur github). Depuis 1989 et jusqu'en 2021, il fut maintenu par une équipe de bénévoles au sein du projet LaTeX3.)

Il a fait des études en mathématiques au Massachusetts Institute of Technology (MIT) puis à l'université Brandeis où il a reçu son Ph.D. (doctorat) de mathématiques en 1972

## 3 Utilisation (Comment ça marche ?)

### 3.1 Introduction

La composiiton d'un texte se décompose en 3 étapes :
- taper son texte
- compiler ses fichers
- Visualiser son document

Il existe deux types d'arguments pour les commandes : 
- L'argument optionel (respectivement ecrit à l'entérieur de "[]") 
-  L'argument Obligatoire (respectivement ecrit à l'entérieur de "{}")

Il suivent une commande Exemple:
\documentclass [A4 paper, 12pt] {book}

### 3.2 Quelques screens

Commencer la création d'un document : 

![légende](IMG/tuto%20latex.png)
![légende](IMG/tuto%20latex2.png)

- Commencer par definir le type de document avec :

\documentclass{article} (ici on créer un article)

- Definir l'auteur, le titre et la date :

\title {bjr}
\author{tomy}
\date{08-03}

- Definir le début et la fin du document : 
\begin {document} 
*Ici le contenus du document 
exemple : {\large Hello :)}*
\end {document}

- Definir 

![légende](IMG)
![légende](IMG)
![légende](IMG)
![légende](IMG)
![légende](IMG)



## 4 Documentation (Où trouver des ressources ?)

### 4.1 En Anglais :

- The LaTeX Project 
- The not so short introduction to LaTeX - Tobias Oetiker, Hubert Partl, Irene Hyna and Elisabeth Schlegl
- LaTeX Tutorial - Derek Banas

### 4.2 En Français :

- GUTenberg (Groupe francophone des Utilisateurs de TEX, LATEX)