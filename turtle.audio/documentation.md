# Turtle Audio



Turtle Audio est un outil de création musicale et graphique en ligne. Il a été créé par Kyle Stetz, un designer et développeur américain. Il utilise la technologie pour résoudre des problèmes et créer des oeuvres d'art numériques. Ses projets sont principalement tournés vers la création musicale alliée à des outils graphiques. Il a notamment développé les outils [typedrummer](http://typedrummer.com/) et [scrrible audio](http://scribble.audio/). 

![landing page](image/screen.png)

L'outil Turtle Audio fonctionne à partir de points et de lignes. Les lignes se crééent à partir d'un double clic puis se poursuivent à partir de certaines lettres du clavier :

    — le m fait avancer la ligne tout droit
    — le r fait tourner la ligne à sa droite à 90°
    — le l fait tourner la ligne à gauche à 90°

