# Tricotteuse & photogrammetrie

*Le but serait de travailler avec du textile pour obtenir l'affiche en question.*

Voilà plusieurs références où design graphique et textile se rencontrent : 

- *Local People (What The F**k)*, Damien Poulain  
*composition "d'affiches" en textile : coutures, formes colorées, typographies*  
! [<https://damienpoulain.com/local-people-what-the-fk/>] [image/DP_Larg_Ibiza7.jpeg]
- *Fragmented memory*, Phillip David Stearn  
*Par un procédé informatique les tapisseries prennent l'images des datas que l'ordinateur envoie à la machine*  
! [<https://phillipstearns.com/artwork#/fragmented-memory/>] [image/Fragmented_Memory_STEARNS_08.jpeg]
- *Surface Proxy*, Clément Valla  
*"patrons" de photogrammetries de divers objets imprimés sur textile : le tissu est ensuite déposé sur l'objet correspondant, rendant ainsi compte de son aspect de manière distordue*   
! [http://clementvalla.com/work/surface-proxy/] [image/Fragmented_Memory_STEARNS_08.jpeg]
- *Spectrogrammes*, Claire Williams  
*Une antenne transmet à un ordinateur les flux electromagnétiques de la pièce qui les envoie ensuite à une machine à tricoter hackée qui rends palpable les variations sonores du lieu (pixels traduits en 0 et 1)*  
! [http://www.xxx-clairewilliams-xxx.com/projets/knitted-spectrogrammes/] [image/045_codeislaw_c_jeanchristophe_lett.jpeg]  

## Tricotteuse

Comme vu avec Claire Williams, il est possible de hacker une tricotteuse pour permettre le dialogue entre la machine et un ordinateur. On pourrait alors utiliser le logiciel **Ayab**. Cependant, bien que possible réaliser ce hack tout seul serait une trop grosse charge de travail, trouver d'autre solution ? Négocier avec un FabLab ? 

## Photogrammétrie

L'utilisation de la photogrammetrie serait très intéressante vis à vis de mon projet de diplôme traitant des "non-lieux", situés à la rencontre des zones urbaines / industrielles / rurales. Rendre compte d'un espace, d'un lieu. 
Utilisation du logiciel **MicMac**, conçu par l'IGN et l'ENSG : idéal pour cartographie. 

<!-- *italique*
**gras** 

-element 1
-element 2

1. element 
2. element 

<lien_cliquable>

[wikipedia](lien du site)

![légende] [lien image/]   -->