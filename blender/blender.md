# Présentation de Blender

*Blender est un logiciel libre de modélisation, d’animation par ordinateur et de rendu en 3D, créé en 1998. Il est actuellement développé par la Fondation Blender.*

## Historique

- **Origine et développement**

Produit à l’origine par le studio d'animation néerlandais NeoGeo BV, fondé en 1988 par Ton Roosendaal et Frank van Beek, la toute première version de Blender est inspirée du logiciel de lancer de rayon Traces, programmé par Ton Roosendaal sur Amiga en 1989.

La première version aboutie de Blender date de janvier 1994, date de la première sauvegarde du logiciel sur la station de travail Indigo2 Silicon Graphics utilisée par Ton Roosendaal. Mais ce n'est qu'en janvier 1998 que le logiciel est publié publiquement comme logiciel gratuit, d'abord pour Unix, puisque développé sur station Silicon Graphics, puis quelques mois plus tard pour Linux et FreeBSD.

Au début de l’année 2002, étant donné le peu de gains qu’ils en retiraient, les investisseurs décident de retirer leur soutien à Ton Roosendaal, ce qui a pour effet immédiat la faillite de la société NaN et la fermeture du site Internet de Blender. Néanmoins, la communauté d’utilisateurs de Blender se reforme rapidement au sein du forum de discussion du site Elysiun (devenu aujourd’hui BlenderArtists).

La fondation commence alors à rendre plus modulaire le code pour faciliter son évolution. On sépare Blender en plusieurs bibliothèques afin d'ouvrir ses fonctionnalités à d’autres logiciels. Blender devient alors le composant standard d'un écosystème qui va en faire une référence.

- **Blender aujourd’hui**

Depuis 2019 le logiciel Blender est de plus en plus reconnu par les entreprises du secteur de l'animation 3D, comme Epic Games, Ubisoft et NVIDIA.

En 2020, la version 2.8 sort avec une révision de l'interface rendant Blender plus facile d'utilisation pour les débutants, et l'ajout d'un moteur de rendu temps réel nommé Eevee.

La libération du code source a donné un élan important au développement du logiciel. Les équipes de développeurs y apportent parfois des fonctionnalités nouvelles (utilisables d'abord dans les versions expérimentales). Les amateurs de Blender peuvent suivre son évolution sur les différentes versions. Selon certains professionnels, sa modularité permet à Blender de rivaliser avec les autres logiciels commerciaux professionnels.

## SUZANNE ! <3

Suzanne est la mascotte de Blender et le plus courant des « modèles d’essai » (comme la théière dans beaucoup de logiciels 3D). C’est un modèle composé de 507 sommets, 1 005 arêtes et 500 faces. Suzanne est souvent utilisée pour tester les matériaux, les textures ou encore les réglages de l’éclairage. Le plus grand concours de Blender décerne **le Prix Suzanne** (sous la forme d’une statuette de Suzanne).
![légende](images/suzanne.png)
*Suzanne*

## Communauté Blender et licences

Blender possède une grande communauté qui a donné naissance à un nombre considérable de productions : didacticiels, plugins, images statiques, courts métrages, bibliothèques de modèles 3D.

**De fait, une licence spéciale a été créée, la Blender Artistic License.**

Elle vise les didacticiels, les fichiers .blend d’exemple ainsi que les images et animations. Elle est plus limitative que la *Blender Documentation License*, mais est pensée pour protéger les droits des auteurs sur leurs didacticiels.
Les auteurs peuvent choisir la *Blender Documentation License*, moins limitative, mais aucune autre que ces deux licences ne sera acceptée pour les didacticiels sur le site de la Fondation Blender.

## Fonctionnalités

Disponible dans de nombreuses langues, Blender est également disponible sur plusieurs plates-formes telles que **Microsoft Windows 8.1 et 10, MacOS, GNU/Linux, IRIX, Solaris, FreeBSD, SkyOS, MorphOS et Pocket PC**. De plus, c’est un programme extensible (ajout de scripts) à l’aide du *langage Python*.

Exemples de ce que **Blender** peut faire : 

1. Gestion de plusieurs techniques de modélisation, comme polygonale, subdivision de surface, courbes de Bézier, surfaces NURBS, metaballs et sculpture numérique.
2. Différents moteurs de rendu compatibles avec le logiciel dont l'exporteur pour POV-Ray inclus de base, ainsi que la possibilité d’exportation pour de nombreux moteurs.
3. Gestion avancée d’animations incluant un système d’armaturage, animation linéaire (par courbes IPO) et non linéaire (par actions), cinématique inverse, déformations par courbes et lattices, Keys Shape (Morphage), contraintes, vertex weighting, corps souples et corps rigides avec gestion des collisions, système de particules (utilisation du moteur physique Bullet).
4. Composition vidéo (séquenceur et timeline gérant les plugins), à laquelle s’ajoute la bande son qui peut être synchronisée en interne.
5. Compositeur nodal d’image, pleinement intégré au flux du rendu.
6. Création avancée de matériaux intégrant un système nodal.
7. Système de développement UV très avancé.
8. Traitement des éclairages avancés par occlusion ambiante et radiosité (cette dernière, n’étant pas liée au ray-tracing, fonctionne aussi en scanline « ligne à ligne »).
9. Langage de script embarqué basé sur le python permettant d’accéder à la plupart des fonctions.
10. Simulation de fluides réalistes, bien que largement en dessous de RealFlow.
11. Simulateur de fumée et de feu.

## Le projet Sprite Fright

Sprite Fright est un court-métrage libre créé par le Blender Studio sorti le 30 octobre 2021. Le projet débute début 2021, avec Matthew Luhn (en) en tant que réalisateur. Le film de 10 minutes met en scène un groupe de cinq adolescents découvrant une communauté de paisibles créatures champignons, qui s'avèrent finalement particulièrement rancunières lorsque l'on touche à la forêt.
![légende](images/affiche.jpeg)
*Affiche du film "Sprite Fright"*

## En conclusion

Blender est avant tout un logiciel destiné à créer des images, animées ou non. Il permet de mettre en scène un modèle, un contexte, et de plonger le spectateur dans un univers. Cela en fait un bon logiciel pour la communication et la création artistique.
En design produit Blender est un bon logiciel pour :

- La réalisation d’avant projets, lorsque l’objectif est avant tout de montrer ce que pourra être le produit.
- La conception de produits aux surfaces organiques et complexes longs à modéliser avec les outils CAO, par exemple des produits destinés à l’impression 3D.
- La mise en scène des produits, en image fixe ou animée. Blender permet de créer des scènes 3D de bonne qualité et des rendus réalistes du produit en contexte.

**Les sources sont :** 

[wikipedia.org](https://wikipedia.com)

[blender.org](https://www.blender.org)

